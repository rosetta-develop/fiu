---
title: Muni magazine
scripts: Latin
typefaces: Skolar
country: Czech Republic
year: 2011–14
---

The [magazine of the Masaryk University](http://www.online.muni.cz/) uses Skolar by David Březina for its body copy and Adelle by TypeTogether for headlines. The magazine was designed by Petr Hrnčíř.

# README – Fonts in use

This folder is tracked with Git for website updates.

Important, "_ source" folders are not tracked by Git.

Note: do not use TAB in Markdown metadata block (e.g. in situations where there are multiple typefaces or scripts), use two spaces instead. Do not refer to type families or scripts which are not in the database.

## Image dimensions

Thumbnail: 560x560px (file size < 120K)
Detail: 1005xYYY and 640xYYY (file size < 120K)
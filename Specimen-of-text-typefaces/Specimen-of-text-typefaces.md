---
title: Specimen of text typefaces
scripts: Latin
typefaces:
  - Aisha
  - Arek
  - Eskorte
  - Huronia
  - Nassim
  - Neacademia
  - Skolar
country: Czech Republic
year: 2014
---

Eight of our text type families presented in a fancy old-school poster designed by Anna Giedryś. It is printed in gold and black colours on Munken Pure and distributed in a transparent sleeve.

*Also presented is an unreleased typeface Prakashan by Alessia Mazzarella.*

---
title: Es ist Zeit für Turnschuhe!
scripts: Latin
typefaces: Skolar
country: Germany
year: 2012
---

Elena Schneider designed a catalogue and promotional meterial for the exhibition Es ist Zeit für Turnschuhe! (Czas na obuwie sportowe!) in Dortmund. She used an unbeatable combo of Skolar, Futura, and shoelaces.

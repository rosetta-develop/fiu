---
title: Multiple Memories 
scripts: Latin
typefaces: Arek
country: Germany
year: 2015
---

The multidisciplinary festival Multiple Memories focuses on the cultural issues of migration. The editorial design and visual identity by [JAC design studio](http://www.jac-gestaltung.de) uses Arek by Khajag Apelian.

*Photos by Hannes Woidich.*


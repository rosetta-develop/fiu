---
title: Jeg har ventet på deg
scripts: Latin
typefaces: Neacademia
country: Norway
year: 2014
---

This novel, situated in the 1950s on a small island along the southern coast, tells a life-changing story of three sisters. The author, Mirjam Kristensen, is an exquisite prosaist and storyteller. The book designed by Frode Bo Helland, is set in Neacademia, and published by Forlaget October.